#!/bin/bash
# https://dnf.readthedocs.io/en/latest/automatic.html
# https://docs.fedoraproject.org/en-US/quick-docs/autoupdates/

enable() {
    dnf install dnf-automatic
    env $EDITOR sudoedit /etc/dnf/automatic.conf
    systemctl enable --now dnf-automatic.timer
    systemctl list-timers dnf-*
}

if [ $(UID) != 0 ];
  then sudo enable
  else enable
fi

